char *error_msg(char *);
char *warning_msg(char *);
char *notice_msg(char *);
char *welcome_msg(char *);
char *pm();
char *serv_msg();
char *concatenate_message( char*, char *);