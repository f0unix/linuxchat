#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "colors.h"

//@TODO 
// function (foreground, background, message type)
// struct definition to set each type


char *error_msg( char *message ){
	char *notif = "\033[31m[ERREUR]\e[0m ";
	return concatenate_message( notif, message );   

}

char *warning_msg( char *message ){

	char *notif = "\e[93m[AVERTISSEMENT]\e[0m ";
	return concatenate_message( notif, message );
    
}

char *notice_msg( char *message ){

	char *notif = "\e[96m[NOTICE]\e[0m ";
	return concatenate_message( notif, message );
    
}

char *welcome_msg( char *message ){

	char *result = malloc(strlen(message)+1);//+1 for the zero-terminator
	strcat(result,"\e[41m");
	strcat(result, message);
	strcat(result,"\e[0m");
	return result;
}

char *pm(){
	char *result = "\e[5m[Message Privée]\e[0m";
	return result;
}


char *concatenate_message( char *notif, char *message ){

	char *result = malloc(strlen(notif)+strlen(message)+1);//+1 for the zero-terminator
    strcpy(result, notif);
    strcat(result, message);
    return result;	
}

