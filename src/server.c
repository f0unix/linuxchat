#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <sys/types.h>
#include "lib/colors/colors.h"

#define MAX_CLIENTS 100

static unsigned int cli_count = 0;
static int identificateur = 110;

/* Client structure */
typedef struct {
    struct sockaddr_in addr;    /* l'addresse du client */
    int connfd;         /* le file descriptor du client */
    int identificateur;            /* identificateur pour client */
    char nom[32];          /* Client name */
} client_t;

client_t *clients[MAX_CLIENTS];

/* ajouter un client a notre list */
void ajout_client(client_t *cl){
    int i;
    for(i=0;i<MAX_CLIENTS;i++){
        if(!clients[i]){
            clients[i] = cl;
            return;
        }
    }
}

/* effacer le client de la liste */
void effacer_client(int identificateur){
    int i;
    for(i=0;i<MAX_CLIENTS;i++){
        if(clients[i]){
            if(clients[i]->identificateur == identificateur){
                clients[i] = NULL;
                return;
            }
        }
    }
}

/* Envoie du message a tous a l'exception de l'envoyeur initiale */
void envoie_message(char *s, int identificateur){
    int i;
    for(i=0;i<MAX_CLIENTS;i++){
        if(clients[i]){
            if(clients[i]->identificateur != identificateur){
                write(clients[i]->connfd, s, strlen(s));
            }
        }
    }
}

/* Envoie des messages vers tous les clients */
void envoie_message_tous(char *s){
    int i;
    for(i=0;i<MAX_CLIENTS;i++){
        if(clients[i]){
            write(clients[i]->connfd, s, strlen(s));
        }
    }
}

/* Envoie du message a l'envoyeur uniquement */
void envoie_message_soi(const char *s, int connfd){
    write(connfd, s, strlen(s));
}

void envoie_aide(int connfd){
    char *s = malloc(80);
    strcat(s, "\e[1;5;91m");
    strcat(s,"=================================\r\n");
    strcat(s, "LISTE DES COMMANDE POSSIBLE\r\n");
    strcat(s,"=================================\r\n");
    strcat(s, "\e[0;0;0m");
    strcat(s,"+--------------------------------+\r\n");
    strcat(s, "\e[38;5;226m\\QUITTER\e[0m        Quitter\r\n");
    strcat(s, "\e[38;5;226m\\PING\e[0m           Tester la presence du serveur\r\n");
    strcat(s, "\e[38;5;226m\\NOM\e[0m            <nom> Changer votre nom \r\n");
    strcat(s, "\e[38;5;226m\\P2P\e[0m            <reference> <message> Envoyer des messages privés a un utilisateur données \r\n");
    strcat(s, "\e[38;5;226m\\LIST\e[0m           Liste des clients actifs\r\n");
    strcat(s, "\e[38;5;226m\\AIDE\e[0m           Liste des commandes\r\n");
    strcat(s,"+--------------------------------+\r\n");
    envoie_message_soi(s, connfd);
    free(s);
}

/* Envoie du message au client  */
void envoie_message_client(char *s, int identificateur){
    int i;
    for(i=0;i<MAX_CLIENTS;i++){
        if(clients[i]){
            if(clients[i]->identificateur == identificateur){
                write(clients[i]->connfd, s, strlen(s));
            }
        }
    }
}

/* Envoie d'une liste des clients actives */
void envoie_clients_actifs(int connfd){
    int i;
    char s[64];
    for( i= 0; i<MAX_CLIENTS;i++){
        if(clients[i]){
            sprintf(s, "CLIENT:: %d | %s\r\n..................\n", clients[i]->identificateur, clients[i]->nom);
            envoie_message_soi(s, connfd);
        }
    }
}

/* Detection du enter */
void strip_newline(char *s){
    while(*s != '\0'){
        if(*s == '\r' || *s == '\n'){
            *s = '\0';
        }
        s++;
    }
}

/* Affichage de l'addresse IP */
void affichage_addr_client(struct sockaddr_in addr){
    printf("%d.%d.%d.%d",
        addr.sin_addr.s_addr & 0xFF,
        (addr.sin_addr.s_addr & 0xFF00)>>8,
        (addr.sin_addr.s_addr & 0xFF0000)>>16,
        (addr.sin_addr.s_addr & 0xFF000000)>>24);
}

/* utilisage des threads dans un client */
void *thread_client(void *arg){
    char buff_out[1024];
    char buff_in[1024];
    int rlen;
    char *tmp_str;

    cli_count++;
    client_t *cli = (client_t *)arg;

    printf("%s", warning_msg("Un client c'est connecté a l'addresse : \e[1;4m") );
    affichage_addr_client(cli->addr);
    printf("\e[0m ::: Identificateur %d\n", cli->identificateur);
    
    sprintf(buff_out, "%s %s\r\n",welcome_msg("Client connecté: "), cli->nom);
    envoie_aide(cli->connfd);
    envoie_message_tous(buff_out);

    /* Recevoir l'info binaire du client via socket */
    while((rlen = read(cli->connfd, buff_in, sizeof(buff_in)-1)) > 0){
            buff_in[rlen] = '\0';
            buff_out[0] = '\0';
        strip_newline(buff_in);

        /* ignorer un buffer vide */
        if(!strlen(buff_in)){
            continue;
        }
    
        /* Special options */
        if(buff_in[0] == '\\'){
            char *command, *param;
            command = strtok(buff_in," ");
            if(!strcmp(command, "\\QUITTER")){
                break;
            }else if(!strcmp(command, "\\PING")){
                envoie_message_soi("\e[44m[SERVEUR]\e[0m \e[92mA votre service\e[0m\r\n",cli->connfd);
            }else if(!strcmp(command, "\\NOM")){
                param = strtok(NULL, " ");
                if(param){
                    char *old_name = strdup(cli->nom);
                    strcpy(cli->nom, param);
                    sprintf(buff_out, "Renommage de %s a %s\r\n", old_name, cli->nom);
                    free(old_name);
                    envoie_message_tous(buff_out);
                }else{
                    envoie_message_soi("\e[44m[SERVEUR]\e[0m \e[92mLe nom doit etre specifié\e[0m \r\n" ,cli->connfd);
                }
            }else if(!strcmp(command, "\\P2P")){
                param = strtok(NULL, " ");
                if(param){
                    int identificateur = atoi(param);
                    param = strtok(NULL, " ");
                    if(param){
                        sprintf(buff_out, "%s[%s]", pm(),cli->nom);
                        while(param != NULL){
                            strcat(buff_out, " ");
                            strcat(buff_out, param);
                            param = strtok(NULL, " ");
                        }
                        strcat(buff_out, "\r\n");
                        envoie_message_client(buff_out, identificateur);
                    }else{
                        envoie_message_soi("\e[44m[SERVEUR]\e[0m \e[92mMais ou est ton message ?\e[0m\r\n", cli->connfd);
                    }
                }else{
                    envoie_message_soi("\e[44m[SERVEUR]\e[0m \e[92mcontact ne peut pas etre vide\e[0m\r\n", cli->connfd);
                }
            }else if(!strcmp(command, "\\LIST")){
                
                sprintf(buff_out, "[CLIENTS] %d\r\n", cli_count);
                envoie_message_soi(buff_out, cli->connfd);
                envoie_clients_actifs(cli->connfd);
            }else if(!strcmp(command, "\\AIDE")){
                // strcat(buff_out, "\\QUITTER     Quitter\r\n");
                // strcat(buff_out, "\\PING     Tester la presence du serveur\r\n");
                // strcat(buff_out, "\\NOM     <nome> Changer votre nom \r\n");
                // strcat(buff_out, "\\P2P  <reference> <message> Envoyer des messages privés a un utilisateur données \r\n");
                // strcat(buff_out, "\\LIST   Liste des clients actifs\r\n");
                // strcat(buff_out, "\\AIDE     Liste des commandes\r\n");
                envoie_aide(cli->connfd);
                // envoie_message_soi(buff_out, cli->connfd);
            }else{
                envoie_message_soi("\e[44m[SERVEUR]\e[0m \e[92m Commande non identifier\e[0m\r\n", cli->connfd);
            }
        }else{
            /* envoie du message */
            sprintf(buff_out, "[%s] %s\r\n", cli->nom, buff_in);
            envoie_message(buff_out, cli->identificateur);
        }
    }

    /* fermeture de la connection */
    close(cli->connfd);
    sprintf(buff_out, "\e[44m[SERVEUR]\e[0m \e[92m, AU REVOIR %s\e[0m\r\n", cli->nom);
    envoie_message_tous(buff_out);

    /* effacer client et detachement du thread */
    effacer_client(cli->identificateur);
    printf("%s", warning_msg("QUITTER "));
    affichage_addr_client(cli->addr);
    printf(" Identificateur : %d\n", cli->identificateur);
    free(cli);
    cli_count--;
    pthread_detach(pthread_self());
    
    return NULL;
}

int main(int argc, char *argv[]){
    int listenfd = 0, connfd = 0;
    int port;
    struct sockaddr_in serv_addr;
    struct sockaddr_in cli_addr;
    pthread_t thread_id;

    
    if( argc != 2 ){
        printf("./server [PORT]\n");
        exit(1);
    }else{
        port = atoi(argv[1]);
    }

    /* parametre de notre socket */
    /* declaration du socket */
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    serv_addr.sin_port = htons(port); 

    /* Soudage */
    if(bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) < 0){
        perror("Le soudage du socket a echouer");
        return 1;
    }

    /* Ecoute */
    if(listen(listenfd, 10) < 0){
        perror("Ecout du socket echouer");
        return 1;
    }

    printf("%s\n", notice_msg("SERVEUR DEMMARRAGE"));

    /* boucle infinie pour accepter les clients */
    while(1){
        socklen_t clilen = sizeof(cli_addr);
        connfd = accept(listenfd, (struct sockaddr*)&cli_addr, &clilen);

        /* verifier que le nombre maximale des clients a ete atteint */
        if((cli_count+1) == MAX_CLIENTS){
            printf("%s\n", error_msg("Nombre Maximal des clients atteint"));
            printf("REJETTER");
            affichage_addr_client(cli_addr);
            printf("\n");
            close(connfd);
            continue;
        }

        /* les parametres des clients */
        // on declare une structure de client et on reserve un espace memoire
        client_t *cli = (client_t *)malloc(sizeof(client_t));

        cli->addr = cli_addr;
        cli->connfd = connfd;
        cli->identificateur = identificateur++;
        sprintf(cli->nom, "%d", cli->identificateur);

        /* ajout d'un client a notre liste des client present et declaration d'un thread*/
        ajout_client(cli);
        pthread_create(&thread_id, NULL, &thread_client, (void*)cli);

        /* Reduce CPU usage */
        sleep(1);
    }
}